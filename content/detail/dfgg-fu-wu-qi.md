---
name: 花风Minecraft服务器
address: play.dfggmc.top:52080
group: 164305079
version: 1.8.x-1.20.x
team: dfgg工作室
lixian: true
---
# dfgg服务器

## 介绍

欢迎来到花风Minecraft服务器！ 
一个有多种玩法的多世界服务器 
已有玩法：插件生存，空岛生存 ，小游戏
已开服一年半，短期不跑路！ 
原生版本支持:Minecraft:Java 1.8-1.12.2 
可通过反代服务器使用高版本进入，最高支持到1.20.x 
官方网站：https://www.dfggmc.top 
论坛：https://bbs.dfggmc.top/t/dfgg-server 
欢迎加入我们！

## 服务器信息

|||
| :---: | :---: |
| 源服务器地址（版本1.8.x-1.12.x） | play.dfggmc.top:58020 |
| 反代服务器地址（版本1.12.x-1.20.x） | proxy.dfggmc.eu.org:13281 |
| 服务器团队 | dfgg Studio |

## 链接

- [官网](https://www.dfggmc.top)
- [论坛](https://bbs.dfggmc.top/t/dfgg-server)